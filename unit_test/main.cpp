#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main()
#include "../lib/Catch.hpp"
#include "../lib/Divider.h"

TEST_CASE( "Divider unit tests", "[Divider]" ) {

	Divider* divider = new Divider;

	// As requested ONLY Divider calculation unit test have been implemented
    REQUIRE( divider->calculate({1,2}) == 0.5 );
    REQUIRE_THROWS( divider->calculate({1,0}));
    REQUIRE_THROWS( divider->calculate({}));


}