# Calculation Engines
Technical C++ exercise written by Jelmer van Lochem. Program performs calculations on a vector of integers. Currently two engines have been implemented, namely: "multiplier" and "divider". Program offers an API whereby extra engines can be implemented.

## Requirements
* CMake > 3.1
* gcc / g++ > 5.0

## Instructions
1. Clone this repository `Test`
2. Create an empty `build` directory with `mkdir build`
3. Run CMake in build directory: `cmake ..` and `make`
4. Run program in `build` directory: `./calc <engine_name> <input>`
or
5. Run unit tests in `build` directory: `./unit_test`

## Usage
For the `calc` program run `./calc <engine_name> <input>`. Supported `engine_name`'s are `multiplier`and `divider`. Supported `input` is a list of integers, for example `1 2 3 4` or a list of .txt filenames without an extension.

## API
Extra engines can be added by creating extra interface and implementation files (see `lib/Divider.h` and `lib/Divider.cpp` as an example). Furthermore to support the new Engine the implementations in `lib/Factory.h` will need to be extended. Lastly the file `CMakeLists.txt` will need to be extended in order to compile using all relevant sources.

