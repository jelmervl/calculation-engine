#ifndef DIVIDER_H
#define DIVIDER_H

#include <iostream>
#include "Engine.h"

class Divider : public Engine{
public:

    /**
    * Constructor
    */
    Divider();

    /**
    * Destructor
    */
    virtual ~Divider();

    /**
    * Calculate a result if possible using the given input data 
    * @param integer vector
    * @return double result
    */
    double calculate(const vector<int>& arr);

};

#endif //DIVIDER_H