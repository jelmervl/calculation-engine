#ifndef ENGINE_H
#define ENGINE_H

#include <iostream>
#include <vector>

using namespace std;

class Engine {
public:

    /**
    * Default Constructor
    */
    Engine(){};

    /**
    * Destructor
    */
    virtual ~Engine(){};

    /**
    * Calculate a result if possible using the given input data 
    * @param vector of integers
    * @return engine result based on input
    */
    virtual double calculate(const vector<int>& arr) = 0;

    /**
    * Check if the current type is supported (requested feature)
    * @param string representation of Engine class
    * @param string representation of input type
    * @return true or false if supported
    */
    static bool supported(const string& engine, const string& inputType);

    /**
    * Return engine class based on string equivalent
    * @param string representation of Engine class
    * @return engine class
    */
    static Engine* get(const string& name);

};

#endif //ENGINE_H