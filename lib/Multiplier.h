#ifndef MULTIPLIER_H
#define MULTIPLIER_H

#include <iostream>
#include "Engine.h"

class Multiplier : public Engine{
public:

    /**
    * Constructor
    */
    Multiplier();

    /**
    * Destructor
    */
    virtual ~Multiplier();

    /**
    * Calculate a result if possible using the given input data 
    * @param integer vector
    * @return double result
    */
    double calculate(const vector<int>& arr);

};

#endif //MULTIPLIER_H