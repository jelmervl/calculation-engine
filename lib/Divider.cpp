#include <iostream>
#include <vector>
#include "divider.h"

using namespace std;

Divider::Divider() {};
Divider::~Divider(){};

double Divider::calculate(const vector<int>& data){
    
    if(data.empty()){
        throw "Empty dataset supplied";
    }

    double result = data[0];

    for(int i = 1; i < data.size(); i++){

        if(data[i] == 0){

            throw "Dataset contains a zero. Division by zero not possible.";

        } else{

            result /= double(data[i]);

        }

    }
    
    return result;

}