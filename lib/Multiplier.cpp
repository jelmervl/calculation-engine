#include "multiplier.h"
#include <vector>
#include <cmath>
#include <limits>

using namespace std;

Multiplier::Multiplier() {};
Multiplier::~Multiplier(){};

double Multiplier::calculate(const vector<int>& data){

    if(data.empty()){
        throw "Empty dataset supplied";
    }

    // compute the max number of digits that double can represent, and save it as threshold
    double max = std::numeric_limits<double>::max();
    double maxDigits = std::floor(log10(max)) ;

    double result = data[0];

    for(int i = 1; i < data.size(); i++){


        double curDigits = log10(result); // current number of digits for product
        double toMultiplyWith = double(data[i]);
        double extraDigits = log10(toMultiplyWith);   // number of digits for the value to be multiplied

        // check possibility of overflow for further multiply
        if(curDigits + extraDigits > maxDigits){

            throw "Requested multiplication might overflow number of available digits for a double. Aborted.";

        } else {

            result *= toMultiplyWith;
        
        }
    }

    return result;
}
