#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;


// Check if a string only contains digits
bool digitString(const string& s){
  return s.find_first_not_of( "0123456789" ) == string::npos;
}

// Check if a file exists
bool fileExists(const string& fileName){
    std::ifstream infile(fileName);
    return infile.good();
}

// Handle line breaks correctly, independent of platform
// https://stackoverflow.com/questions/6089231/getting-std-ifstream-to-handle-lf-cr-and-crlf
std::istream& safeGetline(std::istream& is, std::string& t){

    t.clear();

    // The characters in the stream are read one-by-one using a std::streambuf.
    // That is faster than reading them one-by-one using the std::istream.
    // Code that uses streambuf this way must be guarded by a sentry object.
    // The sentry object performs various tasks,
    // such as thread synchronization and updating the stream state.

    std::istream::sentry se(is, true);
    std::streambuf* sb = is.rdbuf();

    for(;;) {
        int c = sb->sbumpc();
        switch (c) {
        case '\n':
            return is;
        case '\r':
            if(sb->sgetc() == '\n')
                sb->sbumpc();
            return is;
        case EOF:
            // Also handle the case when the last line has no line ending
            if(t.empty())
                is.setstate(std::ios::eofbit);
            return is;
        default:
            t += (char)c;
        }
    }
}

string parameterType(const string& s){

    if(digitString(s)){

        return "list";

    } else if(fileExists(s + ".txt")) {

        return "filenames";

    } else {

        throw "Input parameter is not an integer or the .txt file does not exist.";

    }
}

void getAndValidateInput(string& engineName, string& inputType, vector<int>& data, const int& argc, const char* argv[]){

    if (argc < 3){ // 0 index is filename, 1 index is engine_name and 2+ are filenames or integers
        throw "Please provide at least two arguments: 1. an <engine_name> and 2. a <file_list> or <list of integers>";
    }

    engineName = string(argv[1]);

    // For all file names or integers (or other stuff)
    for(int i = 2; i < argc; ++i){

        // First type determines what the rest should be
        if(i == 2){

            inputType = parameterType(string(argv[i]));  


        //If following parameters are not of same type: error
        } else {

            if(inputType.compare(parameterType(string(argv[i]))) != 0) {

                throw "Input type of parameter does not equal the type of the first parameter.";

            }
        }

        // Input type validated, now validate and get data
        // If files, parse them
        if(inputType.compare("filenames") == 0){

            ifstream file(string(argv[i]) +".txt");
            string line; 

            // For each line
            // Handle line breaks (\r or \n) which do not belong to the current platform
            while (safeGetline(file, line)){

                // Handle last line or empty lines correctly
                if(line.length() == 0){
                    continue;
                }
                
                if(digitString(line)){

                    data.push_back(stoi(line));

                } else {

                    throw "Input file contains a line with data which is not an integer";

                }

            }


        // If integer list add it (already validated that it is an "integer string")
        } else {

            data.push_back(stoi(string(argv[i])));

        }

    } // End foreach input

    // All good

}