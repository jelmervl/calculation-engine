#ifndef FACTORY_H
#define FACTORY_H

#include "Divider.h"
#include "Multiplier.h"
#include "Engine.h"

using namespace std;

// Static method to define which Engines and inputType combinations are supported by the program
// Implemented to meet the requirements of the exercise.

bool Engine::supported(const string& engineName, const string& inputType){

    if(engineName.compare("multiplier") == 0){
        if(inputType.compare("list") == 0){
            return true;
        }
    }

    if(engineName.compare("divider") == 0){
        if(inputType.compare("list") == 0 || inputType.compare("filenames") == 0){
            return true;
        }
    }

    return false;

}

// Static method to return the correct Engine class

Engine* Engine::get(const string &engineName) {

    if (engineName.compare("multiplier") == 0) {

        return new Multiplier();

    } else if (engineName.compare("divider") == 0) {

        return new Divider();

    } else {

        throw "Implemented: \"multiplier\" and \"divider\"";

    }

}

#endif //FACTORY_H