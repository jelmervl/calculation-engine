/**
 * \file main.cpp
 *
 */

#include <iostream>
#include <vector>
#include <string>
#include "../lib/Helpers.h"
#include "../lib/Factory.h"

using namespace std;

// The global main function that is the designated start of the program
int main(int argc, const char* argv[]){

    string engineName;
    string inputType;
    vector<int> data;

    // Check if input is valid, obtain engineName, inputType and data vector
    try{

        getAndValidateInput(engineName,inputType,data,argc,argv);

    } catch(const char* error){
        
        cout << error << endl;
        return 0;

    }  

    // Check if engine name and input type combination is supported
    if(!Engine::supported(engineName,inputType)){

        cout << "Combination of <engine_name> and <input_type> is not supported." << endl;
        return 0;
    }

    // Get Engine based on engine name
    try {

        Engine* engine = Engine::get(engineName);
        double result = engine->calculate(data);
        cout << result << endl;

    } catch(const char *error){
        
        cout << error << endl;

    }   
    
    return 0;

}
